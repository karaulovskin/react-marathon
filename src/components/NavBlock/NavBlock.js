import React from 'react';
import s from './NavBlock.module.scss';

const NavBlock = ({ children }) => {
    return (
        <div className={s.nav}>
            { children }
        </div>
    )
}

export default NavBlock