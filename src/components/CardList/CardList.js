import React, { Component } from 'react';
import Card from '../Card/Card';
import { Input } from 'antd';
import getTranslateWord from '../../services/dictionary';
import s from './CardList.module.scss'

const { Search } = Input;

class CardList extends Component {
    state = {
        label: '',
        value: '',
        isBusy: false
    };

    handleInputChange = (e) => {
        this.setState({
            value: e.target.value
        })
    };

    getTheWord = async () => {
        const value = this.state;
        const getWord = await getTranslateWord(this.state.value);
        const translateWord = getWord.translate;

        this.props.handleAddedItem(value, translateWord);

        this.setState({
            label: `${value.value} - ${translateWord}`,
            value: '',
            isBusy: false
        })
    }

    handleSubmitForm = async () => {
        this.setState({
            isBusy: true
        }, this.getTheWord)
    };

    render() {
        const {item = [], handleDeletedItem } = this.props;
        const { value, label, isBusy } = this.state;
        return (
            <>
                <div className={s.addWords}>
                    { label }
                </div>
                <div className={s.form}>
                    <Search
                        placeholder="input search text"
                        enterButton="Search"
                        size="large"
                        value={value}
                        loading={isBusy}
                        onChange={this.handleInputChange}
                        onSearch={this.handleSubmitForm}
                    />
                </div>
                <div className={s.root}>
                    {
                        item.map(({ eng, rus, id }) => (
                            <Card
                                handleDeleted={
                                    () => {
                                        handleDeletedItem(id);
                                    }
                                }
                                key={ id }
                                eng={ eng }
                                rus={ rus }
                            />
                        ))
                    }
                </div>
            </>
        )
    }
}

export default CardList;