import React, { Component } from 'react';
import FirebaseContext from "../../context/firebaseContext";

class AboutPage extends Component {

    render() {
        return (
            <>
                <h1>Немного о приложении ...</h1>
            </>
        )
    }
}
AboutPage.contextType = FirebaseContext;

export default AboutPage;