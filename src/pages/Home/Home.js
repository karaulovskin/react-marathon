import React, { Component } from 'react';
import Firebase from "../../services/firebase";
import HeaderBlock from '../../components/HeaderBlock/HeaderBlock';
import ContentBlock from '../../components/ContentBlock/ContentBlock';
import FooterBlock from '../../components/FooterBlock/FooterBlock';
import Title from '../../components/Title/Title';
import Paragraph from '../../components/Paragraph/Paragraph';
import CardList from '../../components/CardList/CardList';
import FirebaseContext from "../../context/firebaseContext";

class HomePage extends Component {
    state = {
        wordsArr: [],
    };

    componentDidMount() {
        const { getUserCardsRef } = this.context;

        getUserCardsRef().on('value',res => {
            this.setState({
                wordsArr: res.val() || [],
            })
        });
    }

    handleAddedItem = ({value}, translateWord) => {
        const { wordsArr } = this.state;
        const { database, getUserCardsRef } = this.context;

        database.ref(getUserCardsRef()).set([...wordsArr, {
            id: +new Date(),
            eng: value,
            rus: translateWord
        }])
    }

    handleDeletedItem = (id) => {
        const { wordsArr } = this.state;
        const newWordArr = wordsArr.filter(item => item.id !== id);
        const { database, getUserCardsRef } = this.context;

        database.ref(getUserCardsRef()).set(newWordArr);
    };

    render() {
        const { wordsArr } = this.state;
        return (
            <>
                <div>
                    <HeaderBlock>
                        <Title>
                            React maraphon
                        </Title>
                    </HeaderBlock>
                    <HeaderBlock hideBackground>
                        <Title>
                            Начать учить английский просто
                        </Title>
                        <Paragraph>
                            Кликай по карточкам и узнавай новые слова, быстро и легко!
                        </Paragraph>
                    </HeaderBlock>
                </div>
                <ContentBlock>
                    <CardList
                        handleDeletedItem={this.handleDeletedItem}
                        handleAddedItem={this.handleAddedItem}
                        item={ wordsArr }
                    />
                </ContentBlock>
                <FooterBlock
                    copyright="© 2020 Karaulovskin"
                />
            </>
        )
    }
}
HomePage.contextType = FirebaseContext;

export default HomePage;
