import React, { Component } from 'react';
import { Spin } from 'antd';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import HomePage from './pages/Home/Home';
import AboutPage from "./pages/About/About";
import LoginPage from './pages/Login/Login';
import NavBlock from './components/NavBlock/NavBlock';
import s from './App.module.scss';
import FirebaseContext from './context/firebaseContext';
import { PrivateRoute } from './utils/privateRoute';

class App extends Component {
    state = {
        user: null,
    };

    componentDidMount() {
        const { auth, setUserUid } = this.context;

        auth.onAuthStateChanged((user) => {
            if (user) {
                setUserUid(user.uid);
                localStorage.setItem('user', JSON.stringify(user.uid));
                this.setState({
                    user,
                });
            } else {
                setUserUid(null);
                localStorage.removeItem('user');
                this.setState({
                    user: false,
                });
            }
        })
    }

    render() {
        const { user } = this.state;

        if(user === null) {
            return (
                <div className={s.loader_wrap}>
                    <Spin size="large" />
                </div>
            );
        }

        return (
            <BrowserRouter>
                <Route path="/login" component={LoginPage} />
                <Route
                    render={(props) => {
                        console.log('#props: ', props);
                        const { history: { push } } = props;
                        return (
                            <div>
                                <NavBlock>
                                    <div>
                                        <Link to="/">Home</Link>
                                    </div>
                                    <div>
                                        <Link to="/about">About</Link>
                                    </div>
                                </NavBlock>
                                <div>
                                    <PrivateRoute path="/" exact component={HomePage} />
                                    <PrivateRoute path="/home" component={HomePage} />
                                    <PrivateRoute path="/about" component={AboutPage} />
                                </div>
                            </div>
                        )
                    }}
                />
            </BrowserRouter>

            // <>
            //     { user ? <HomePage user={user} /> : <LoginPage /> }
            // </>
        );
    }
}
App.contextType = FirebaseContext;

export default App;
